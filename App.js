import React, { Component } from 'react';
import { StyleSheet, Text, TextInput, View, Button, Alert } from 'react-native';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = { f1: '', f2: '', result: '' };
  }

  render() {
    // var f1 = this.state.f1;
    // var f2 = this.state.f2;
    let { f1, f2 } = this.state; 
    const result = f1 && f2 ? f2 + f1 : null;

    return (
      <View style={{ flex: 1, alignItems: 'center' }}>
        <TextInput
        style={{ borderBottomWidth : 1.0, width: 180 }}
        keyboardType='numeric'
        onChangeText={(text) => this.setState({f1: parseInt(text)}) }
        />

        <TextInput
        style={{ borderBottomWidth : 1.0, width: 180, marginBottom: 40 }}
        keyboardType='numeric'
        onChangeText={(text) => this.setState({f2: parseInt(text)}) }
        />

        <Button
        title="Add Numbers"
        onPress={() => Alert.alert('Sum is: ' + result)}
        />

        {/* { result ? <Text
        style={{ fontSize: 22 }}
        >Sum is: { result }</Text> : null } */}
      </View>
    );
  }

}

export default App;